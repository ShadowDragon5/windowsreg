""" Apdorojami log2timeline duomenys """
""" Python 3.7.2 """

import re
from datetime import datetime
import sys

with open("../result/res1.out", "w", encoding="utf-8") as fileOut:
    with open("../data/1st_ex3.l2tcsv", "r", encoding="latin1") as fileIn:
    #with open(sys.argv[1], "r", encoding="utf-8") as fileIn:
        header = fileIn.readline().strip().split(',')

        iDate = header.index("date")
        iTime = header.index("time")
        iMACB = header.index("MACB")
        iSource = header.index("source")
        iSourceType = header.index("sourcetype")
        iType = header.index("type")
        iShort = header.index("short")
        iFileName = header.index("filename")
        iDesc = header.index("desc")
        iVisit = len(header)

        fileOut.write(",".join([
            "line",
            header[iDate],
            header[iTime],
            header[iSource],
            header[iShort],
            "visit",
            ]) + '\n')

        prevShort = ""
        prevData = None
        prevWEB = None
        sourceTypes = []
        for i, line in enumerate(fileIn):
            data = line.strip().split(',')  # list

# yra visa informacija
            if (len(data) != len(header)):
                continue

# LOG LNK (ii)
            if (data[iSource] == "LOG" or data[iSource] == "LNK"):
                # Time and date matches
                if ((prevData is not None) and
                    prevData[iDate] == data[iDate] and
                    prevData[iTime] == data[iTime] and
                    (prevData[iSource] == "LOG" or
                    prevData[iSource] == "LNK")):
                    prevData = data
                    continue

            elif ((prevData is not None) and
                prevData[iDate] == data[iDate] and
                prevData[iTime] == data[iTime]):

                prevData = data
                continue

# MACB (iii)
            if (data[iMACB][3] != 'B'):
                continue

            if (data[iSource] == "FILE"):
                continue
# a.
            if (data[iSource] == "LOG"):
                macaddr = data[iShort].strip().split(' ')
                mac = re.search(r"-([^-]+?)$", macaddr[0])
                if (mac):
                    macaddr[0] = mac[1]
                    data[iShort] = " ".join(macaddr)

# b.
            if (data[iSource] == "LNK"):
                short = re.search(r"\[(?P<de>.+?)(\] (?P<pt>.+)|$)",
                    data[iShort])
                #print("\033[31m" + short[0] + "\033[0m")
                if (short["pt"] and
                    ("Empty" in short["de"] or
                    "..." not in short["pt"])):
                    data[iShort] = short["pt"]
                else:
                    data[iShort] = short["de"]

# c.
            if (data[iSource] == "META"):
                data[iShort] = data[iFileName]

# d.
            if (data[iSource] == "OLECF"):
                data[iShort] = data[iFileName]

                if ((prevData is not None) and
                    prevData[iFileName] == data[iFileName] and
                    prevData[iSource] == "OLECF"):
                    prevData = data
                    continue

# e.
            if (data[iSource] == "PE" and data[iShort] == "pe_type"):
                data[iShort] = data[iFileName]
                if (re.match(r"\.exe$", data[iFileName]) == None):
                    prevData = data
                    continue

# iv.
            if (data[iSource] == "WEBHIST"):
                if (re.match(r".*(H|h)istory.*", data[iSourceType]) == None):
                    continue

                short = re.search("\w+://[^/]+/", data[iShort])
                data[iShort] = short[0] if short != None else data[iShort]
                #print(short)
                if ((prevWEB is not None) and
                    prevWEB[iShort] == data[iShort] and
                    math.abs(
                    (datetime.striptime(date[iDate] + ' ' + date[iTime],
                    '%m/%d/%Y %H:%M:%S') -
                    datetime.striptime(prevWEB[iDate] + ' ' + prevWEB[iTime],
                    '%m/%d/%Y %H:%M:%S')).minutes) < 50):
                    continue

                visit = ""
                if (re.match(r"File Downloaded", data[iType])):
                    visit = "Download"
                    #print(data[iShort])
                    #short = re.search(r"(.+?)( d?.*$|$)", data[iShort])

                if (re.match(r".*Chrome.*", data[iSourceType]) != None):
                    try:
                        visit = re.search(r"Type: \[([^\] ]*)", data[iDesc])[1]
                    except:
                        visit = visit

                    tCount = re.search(r"type count ([0-9]+)", data[iDesc])
                    if (tCount is not None):
                        c = int(tCount[1])
                        visit += " " + str(c) if c > 0 else ""

                if (re.match(r".*Firefox.*", data[iSourceType]) != None):
                    try:
                        visit = re.search(r"Transition: ([^ ]*)",
                            data[iDesc])[1]
                    except:
                        visit = visit

                    if ((visit == "LINK") and
                        re.search(r"pdf", data[iDesc], re.IGNORECASE) is not
                            None):
                        visit = "PDF"

                    tCount = re.search(r"\[count: ([0-9]+)", data[iDesc])
                    if (tCount is not None):
                        #print(tCount[0])
                        c = int(tCount[1])
                        visit += " " + str(c) if c > 0 else ""

                    #print(visit)

                if (re.match(r"mail", data[iShort]) is not None):
                    visit = "mail"
                # visit
                data.append(visit)


# Kartojasi "short" laukas
            if (prevShort == data[iShort]):
                continue
            else:
                prevShort = data[iShort]
# END
            prevData = data

            fileOut.write(",".join([
                str(i + 2),
                data[iDate],
                data[iTime],
                data[iSource],
                #data[iMACB],
                data[iShort],
                data[iVisit] if data[iSource] == "WEBHIST" else '',
                ]) + '\n')
