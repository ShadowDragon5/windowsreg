import fileinput
import urllib.request
import re
#from Levenshtein import distance
from sklearn.cluster import AffinityPropagation
from sklearn.feature_extraction.text import TfidfVectorizer

def get_meta_tags(url):
    out={}
    html = urllib.request.urlopen(url)
    #return html.info()
    html = html.read()
    try:
        html = html.decode("utf-8")
    except:
        html = html.decode("1257")

    m = re.findall("name=\"([^\"]*)\" content=\"([^\"]*)\"",html)
    for i in m:
        out[i[0]] = i[1]
    return out


if (__name__ == '__main__'):

    vectorizer = TfidfVectorizer()

    #data = fileinput.input()
    with open('../data/url_short.txt', 'r') as data:
        d = []
        for x in range(500):
            d.append(data.readline().strip())
        #print(d)
        X = vectorizer.fit_transform(d)
        similarity_matrix = (X * X.T).A
        print(similarity_matrix)
        af = AffinityPropagation(affinity="precomputed", damping=0.79)
        af.fit(similarity_matrix)
        print(X.shape)
        for i in af.cluster_centers_indices_:
            print(d[i])

        #for line in data:
        '''
        for x in range(10):
            line = data.readline().strip()
            print(x, line, flush=True)
            '''

